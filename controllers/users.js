const express = require('express');

function list(req, res, next) {
    res.send('Respond with list');
  };

function index(req, res, next) {
  const id = req.params.id;
  res.send(`Index Parametros => ${id}`);

};

function create(req, res, next) {
  const name = req.body.name;
  const lastName= req.body.lastName;
  res.send(`Create => parametros ${name}, ${lastName}`);
};


function replace(req, res, next) {
  res.send('Respond with replace');
};

function update(req, res, next) {
  res.send('Respond with update');
};

function destroy(req, res, next) {
  res.send('Respond with destroy');
};


module.exports = { list, index, create, replace, update, destroy };