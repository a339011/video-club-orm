const express = require('express');
const { Copy } = require('../db');

function list(req, res, next) {
    Copy.findAll({include:['movie']}).then(objects => res.json(objects)).catch(err => res.send(err));
    };

function index(req, res, next) {
  const id = req.params.id;
  Copy.findByPk(id).then(object => res.json(object))
                    .catch(err => res.send(err));

};

function create(req, res, next) {
  
  const number = req.body.number;
  const format = req.body.format;
  const status = req.body.status;
  const movieId = req.body.movieId;

  let copy = new Object({
    number:number,
    format:format,
    status:status,
    movieId:movieId
  });

  Copy.create(copy)
       .then(obj => res.json(obj))
       .catch(err => res.send(err));

  };

function replace(req, res, next) {
  const id = req.params.id;

  Copy.findByPk(id).then( (object) => {
    const number = req.body.number ? req.body.number : "";
    const status = req.body.status  ? req.body.status : null;
    const format = req.body.format ? req.body.format : null;
    const movieId = req.body.movieId ? req.body.movieId : null;

    object.update({number:number, status:status, format:format, movieId:movieId})
          .then(Copy => res.json(Copy))
          .catch(err => res.send(err));

  }).catch(err => res.send(err));

};

function update(req, res, next) {
  const id = req.params.id;

  Copy.findByPk(id).then( (object) => {
    const number = req.body.number ? req.body.number : object.number;
    const status = req.body.status  ? req.body.status : object.status;
    const format = req.body.format ? req.body.format : object.format;
    const movieId = req.body.movieId ? req.body.movieId : object.movieId;

    object.update({number:number, status:status, format:format, movieId:movieId})
          .then(Copy => res.json(Copy))
          .catch(err => res.send(err));

  }).catch(err => res.send(err));
  
};

function destroy(req, res, next) {
  const id = req.params.id;
  Copy.destroy({where:{id:id}})
        .then(obj => res.json(obj))
        .catch(err => res.send(err))

};


module.exports = { list, index, create, replace, update, destroy };